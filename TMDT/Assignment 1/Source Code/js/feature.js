function showProductList(list) {
	console.log(list);
}
var productBuyed = function (id) {
	this.src = id;
	this.id = '';
	this.name = '';
	this.price = '';
	this.getInfor();
}
productBuyed.prototype.getInfor = function () {
	var continueValue = this.src.length;
	for (var i = this.src.length - 1 ; i >=0 ; i--) {
		if(this.src[i] =='.') {
			continueValue = i;
			break;
		}
	}
	for (var i = continueValue - 1; i >=0 ; i--) {
		if(this.src[i]=='-') {
			continueValue = i;
			break;
		}
		this.price = this.src[i] + this.price;
	}
	for (var i = continueValue - 1; i >=0 ; i--) {
		if(this.src[i]=='-') {
			continueValue = i;
			break;
		}
		this.name = this.src[i] + this.name;
	}
	for (var i = continueValue - 1; i >=0 ; i--) {
		if(this.src[i]=='/') {
			continueValue = i;
			break;
		}
		this.id = this.src[i] + this.id;
	}
}
var productBuyedList = [];
var canvas = new fabric.Canvas('canvas');
window.addEventListener('keydown',this.check,false);
function check(e) {
	if(e.keyCode == 46) {
		// if(confirm('Bạn muốn xóa sản phẩm')){
			var elementDeleSrc = canvas.getActiveObject()._originalElement.currentSrc;
			for (var i = 0; i < productBuyedList.length; i++) {
				if(productBuyedList[i].src == elementDeleSrc) {
					productBuyedList.splice(i,1);
					break;
				}
			}
			canvas.getActiveObject().remove();
			CreateTablePrice();
		// }
	}
}
function getSrc(id) {
	if (id.indexOf('floor') === -1 ) {
		fabric.Image.fromURL(id.replace('3d','2d'), function(oImg) {
			oImg.set({
				width:200,
				height:200,
				left: 20,
				top: 20
			});
			canvas.add(oImg);
			var product = new productBuyed(id.replace('3d','2d'));
			productBuyedList.push(product);
			showProductList(productBuyedList);
			CreateTablePrice();
		});
	}
	else {
		$("#right-panel").css('background-image', "url(" + id.replace('3d','2d') + ")");
	}
}
$("select").change(function() {
	furniture = $("select").val();
	var val = $("select").val();
	x = document.getElementsByClassName(val);
	k = x;
	var img_src = []
	$("#furniture-zone").empty();
	for (var i = 0; i < k.length; i++) {
		var src = x[i].src;
		src = src.slice(84, src.length-4);
		$("#furniture-zone").append("<img src='" + x[i].src + "' width='110px' height='110px' class='draggable' onclick=\"getSrc(\'"+x[i].src+"\')\" />");
		img_src.push(x[i].src);
	}
});
$("#detail").click(function() {
	if ($("#price-table").css("height") == "25px") {
		$("#price-table").css("height","auto");
	}
	else {
		$("#price-table").css("height","25px");
	}
});
function CreateTablePrice() {
	$("#price-table table tbody").empty();
	var total_price = 0;
	for (var i = 0; i < productBuyedList.length; i++) {
		$("#price-table table tbody").append("<tr><td>" + productBuyedList[i].id + "</td><td>" + productBuyedList[i].name + "</td><td>" + parseInt(productBuyedList[i].price) * 1000 + "</td></tr>");
		total_price += parseInt(productBuyedList[i].price) * 1000;
	}
	$('#total-price').text(total_price);
}