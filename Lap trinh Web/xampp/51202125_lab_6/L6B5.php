<!DOCTYPE html>
<html>
<head>
<title>Lab 6 - Lap Trinh Web</title>
<link rel="stylesheet" type="text/css" href="./L6B5.css">
</head>
<body>
	<p>Máy tính cơ bản, thực hiện các phép tính gồm</p>
	<ul>
		<li>Cộng '+'</li>
		<li>Trừ '-'</li>
		<li>Nhân '*'</li>
		<li>Chia '/'</li>
		<li>Lũy thừa '^'</li>
		<li>Tìm phần dư '%'</li>
	</ul>
	<form method="POST">
		<table>
			<tr>
				<td>Số thứ nhất</td>
				<td>Phép tính</td>
				<td>Số thứ 2</td>
			</tr>
			<tr>
				<td><input type="text" name="num1" placeholder="Số thứ nhất"></td>
				<td><input type="text" name="operator" placeholder="Phép tính"></td>
				<td><input type="text" name="num2" placeholder="Số thứ hai"></td>
			</tr>
		</table>



		<input type="submit" name="Submit" value="=">
	</form>
<?php
function checkInput($input) {
	if ($input == '0') {
		return true;
	} else {
		$input = ( float ) $input;
		if ($input == 0) {
			echo "Dữ liệu không hợp lệ";
			return false;
		} else {
			return true;
		}
	}
}
function checkOperator($input) {
	if ($input == '+' || $input == '-' || $input == '*' || $input == '/' || $input == '^' || $input == '%')
		return true;
	echo "Phép tính không hợp lệ hoặc chưa thiết lập";
	return false;
}

$n1 = $_POST ['num1'];
$n2 = $_POST ['num2'];
$operator = $_POST ['operator'];

if (checkInput ( $n1 ) == 1 && checkInput ( $n2 ) == 1 && checkOperator ( $operator ) == 1) {
	$result = 0;
	switch ($operator) {
		case '+' :
			$result = $n1 + $n2;
			break;
		case '-' :
			$result = $n1 - $n2;
			break;
		case '*' :
			$result = $n1 * $n2;
			break;
		case '/' :
			$result = $n1 / $n2;
			break;
		case '^' :
			$result = $n1 ** $n2;
			break;
		case '%' :
			$result = $n1 % $n2;
			break;
		default :
			break;
	}
	echo "<input type='text' name='result' value='" . $result . "'>";
	}
?>
</body>
</html>