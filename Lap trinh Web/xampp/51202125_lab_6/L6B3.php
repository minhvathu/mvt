<!DOCTYPE html>
<html>
<head>
<title>Lab 6 - Lap Trinh Web</title>
</head>
<body>
<?php
echo "For loop: ";
for($i = 0; $i < 100; $i ++) {
	if ($i % 2 != 0) {
		echo $i . " ";
	}
}
echo "<br />";
echo "While loop: ";
$j = 0;
while ( $j < 100 ) {
	if ($j % 2 != 0) {
		echo $j . " ";
	}
	$j ++;
}
?>
</body>
</html>