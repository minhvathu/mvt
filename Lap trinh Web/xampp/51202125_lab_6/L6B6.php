<!DOCTYPE HTML>
<html>
<head>
<title>Lab 6 - Lap Trinh Web</title>
<link rel="stylesheet" type="text/css" href="./L6B5.css">
<style>
.error {
	color: #FF0000;
}

.warning {
	color: #0077CC;
}

#form {
	float: left;
	width: 50%;
}

#result {
	float: left;
}
</style>
</head>
<body>
	<p>Form đăng nhập cơ bản</p>
<?php
// define variables and set to empty values
$firstNameErr = $lastNameErr = $emailErr = $passErr = $birthdayErr = $genderErr = $aboutLimitErr = "";
$firstName = $lastName = $email = $password = $dayBirth = $monthBirth = $yearBirth = $gender = $about = "";
$countComplete = 0;
$complete = "";

if ($_SERVER ["REQUEST_METHOD"] == "POST") {
	$postFirstName = $_POST ["firstName"];
	if (empty ( $postFirstName )) {
		$firstNameErr = "first name is required";
	} elseif (strlen ( $postFirstName ) < 2) {
		$firstNameErr = "first name length less than 2";
	} elseif (strlen ( $postFirstName ) > 30) {
		$firstNameErr = "first name length more than 30";
	} else {
		$firstName = test_input ( $postFirstName );
		$countComplete ++;
	}
	
	$postLastName = $_POST ["lastName"];
	if (empty ( $postLastName )) {
		$lastNameErr = "last name is required";
	} elseif (strlen ( $postLastName ) < 2) {
		$lastNameErr = "last name length less than 2";
	} elseif (strlen ( $postLastName ) > 30) {
		$lastNameErr = "last name length more than 30";
	} else {
		$lastName = test_input ( $postLastName );
		$countComplete ++;
	}
	
	$postEmail = $_POST ["email"];
	if (empty ( $postEmail )) {
		$emailErr = "email is required";
	} elseif (! filter_var ( $postEmail, FILTER_VALIDATE_EMAIL )) {
		$emailErr = "invalid email detected";
	} else {
		$email = test_input ( $postEmail );
		$countComplete ++;
	}
	
	$postPassword = $_POST ["password"];
	if (empty ( $postPassword )) {
		$passErr = "password is required";
	} elseif (strlen ( $postPassword ) < 2) {
		$passErr = "password length less than 2";
	} elseif (strlen ( $postPassword ) > 30) {
		$passErr = "password length more than 30";
	} else {
		$password = test_input ( $postPassword );
		$countComplete ++;
	}
	
	$postDayBirth = $_POST ["dayBirth"];
	$postMonthBirth = $_POST ["monthBirth"];
	$postYearBirth = $_POST ["yearBirth"];
	if (! checkdate ( $postMonthBirth, $postDayBirth, $postYearBirth )) {
		$birthdayErr = "invalid date!";
	} else {
		$dayBirth = $postDayBirth;
		$monthBirth = $postMonthBirth;
		$yearBirth = $postYearBirth;
		$countComplete ++;
	}
	
	if (empty ( $_POST ["gender"] )) {
		$genderErr = "gender is required";
	} else {
		$gender = test_input ( $_POST ["gender"] );
		$countComplete ++;
	}
	
	if (strlen ( $_POST ["about"] ) > 100000) {
		$aboutLimitErr = "text too long";
	} else {
		$about = test_input ( $_POST ["about"] );
		$countComplete ++;
	}
	
	if ($countComplete == 7) {
		$complete = "Complete";
	}
}
function test_input($data) {
	$data = trim ( $data );
	$data = stripslashes ( $data );
	$data = htmlspecialchars ( $data );
	return $data;
}
?>
<div>
		<div id="form">
			<h2>Form Register</h2>
			<p>
				<span class="warning">* required field.</span>
			</p>
			<form method="post"
				action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
				First name: <input type="text" name="firstName"> <span class="error"><?php echo $firstNameErr;?></span><br>
				<span class="warning">* from 2 to 30 characters</span> <br> <br>
				Last name: <input type="text" name="lastName"> <span class="error"><?php echo $lastNameErr;?></span><br>
				<span class="warning">* from 2 to 30 characters</span> <br> <br>
				Email: <input type="text" name="email"> <span class="error"><?php echo $emailErr;?></span><br>
				<span class="warning">* form: something@something.something</span> <br>
				<br> Password: <input type="password" name="password"> <span
					class="error"><?php echo $passErr;?></span><br> <span
					class="warning">* from 2 to 30 characters</span> <br> <br>
				Birthday: <br> Day: <select name="dayBirth">
    <?php for ($i=1; $i <= 31; $i++): ?>
      <option><?php echo $i; ?></option>
    <?php endfor; ?>
  </select> Month: <select name="monthBirth">
    <?php for ($i=1; $i <= 12; $i++): ?>
      <option><?php echo $i; ?></option>
    <?php endfor; ?>
  </select> Year: <select name="yearBirth">
    <?php for ($i=date("Y"); $i >= date("Y")  - 100; $i--): ?>
      <option><?php echo $i; ?></option>
    <?php endfor; ?>
  </select> <span class="error"><?php echo $birthdayErr;?></span><br> <span
					class="warning">*please choose valid date </span> <br> <br> Gender:
				<input type="radio" name="gender" value="male"> Male <input
					type="radio" name="gender" value="female"> Female <input
					type="radio" name="gender" value="other"> Other <span class="error"><?php echo $genderErr;?></span><br>
				<span class="warning">* gender is required</span> <br> <br> Country:
				<select name="country">
					<option>Vietnam</option>
					<option>Australia</option>
					<option>United States</option>
					<option>India</option>
					<option>Other</option>
				</select> <br> <br> About:
				<textarea name="about" maxlength="100000"></textarea>
				<br> <span class="error"><?php echo $aboutLimitErr;?></span> <span
					class="warning">*Limit 100.000 characters!</span> <br> <br> <input
					type="submit" name="submit" value="Submit">
				<button type="reset" value="Reset">Reset</button>
				<strong><?php echo $complete;?></strong>
			</form>
		</div>
		<div id="result">
<?php
echo "<h2>Your Input:</h2>";
echo "FirstName: " . $firstName;
echo "<br>";
echo "LastName: " . $lastName;
echo "<br>";
echo "Email: " . $email;
echo "<br>";
echo "Password: " . $password;
echo "<br>";
echo "DayBirth: " . $dayBirth;
echo "<br>";
echo "MonthBirth: " . $monthBirth;
echo "<br>";
echo "YearBirth: " . $yearBirth;
echo "<br>";
echo "Gender: " . $gender;
echo "<br>";
echo "About: " . $about;
echo "<br>";
?>
</div>
	</div>
</body>
</html>